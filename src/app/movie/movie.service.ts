import axios, { AxiosError } from 'axios';
import { UseQueryOptions, useQuery } from 'react-query';

import { MovieList } from '@/app/movie/movie.types';

const movieKeys = {
  all: () => ['movieService'] as const,
  movies: ({ page, size }: { page?: number; size?: number }) =>
    [...movieKeys.all(), 'movies', { page, size }] as const,
  movie: ({ id }: { id?: number }) =>
    [...movieKeys.all(), 'movie', { id }] as const,
};

export const useMovieList = (
  { page = 0, size = 20 } = {},
  config: UseQueryOptions<
    MovieList,
    AxiosError,
    MovieList,
    InferQueryKey<typeof movieKeys.movies>
  > = {}
) => {
  return useQuery(
    movieKeys.movies({ page, size }),
    (): Promise<MovieList> => axios.get('/moviesa'),
    {
      keepPreviousData: true,
      ...config,
    }
  );
};
