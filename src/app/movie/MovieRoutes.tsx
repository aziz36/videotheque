import React from 'react';

import { Route, Routes } from 'react-router-dom';

import { PageMovie } from '@/app/movie/PageMovie';
import { AuthenticatedRouteGuard } from '@/app/router/guards';
import { Error404 } from '@/errors';

const MovieRoutes = () => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <AuthenticatedRouteGuard>
            <PageMovie />
          </AuthenticatedRouteGuard>
        }
      />{' '}
      <Route path="*" element={<Error404 />} />
    </Routes>
  );
};

export default MovieRoutes;
