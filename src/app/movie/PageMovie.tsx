import {
  Badge,
  Box,
  Button,
  Center,
  Image,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spinner,
  Stack,
  useDisclosure,
} from '@chakra-ui/react';

import { Page, PageContent } from '@/app/layout';
import { useMovieList } from '@/app/movie/movie.service';
import { usePaginationFromUrl } from '@/app/router';
import {
  Pagination,
  PaginationButtonFirstPage,
  PaginationButtonLastPage,
  PaginationButtonNextPage,
  PaginationButtonPrevPage,
  PaginationInfo,
} from '@/components';

export const PageMovie = () => {
  const { page, setPage } = usePaginationFromUrl();

  const pageSize = 5;
  const { data: movies, isLoading: isLoadingPage } = useMovieList({
    page: page - 1,
    size: pageSize,
  });

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Page>
      <PageContent>
        {/* Cas pendant le chargement des films */}
        {isLoadingPage && (
          <Center flex="1">
            <Spinner />
          </Center>
        )}

        {/* Cas lorsqu'il y a des films de chargés */}
        {!isLoadingPage && movies?.totalItems && (
          <>
            <Box>
              <Stack direction="column" is-reversed spacing={5}>
                {movies?.content?.map(
                  ({ id, name, duration, ageLimit, description }) => (
                    <Box
                      key={id}
                      minW="0"
                      maxW="sm"
                      borderWidth="1px"
                      borderRadius="lg"
                      overflow="hidden"
                    >
                      <Image src="https://bit.ly/2Z4KKcF" />
                      <Box p="6">
                        <Box display="flex" alignItems="baseline">
                          <Box
                            color="gray.500"
                            fontWeight="semibold"
                            letterSpacing="wide"
                            fontSize="xs"
                            textTransform="uppercase"
                            ml="2"
                          >
                            <Badge
                              borderRadius="full"
                              px="2"
                              colorScheme="teal"
                            >
                              {}
                            </Badge>
                            {/* box des categories parcourir la liste des categories*/}
                          </Box>
                        </Box>

                        <Box
                          mt="1"
                          fontWeight="semibold"
                          as="h4"
                          lineHeight="tight"
                          noOfLines={1}
                        >
                          {name}
                        </Box>

                        <Box>
                          {duration}
                          <Box as="span" color="gray.600" fontSize="sm">
                            min
                          </Box>
                        </Box>
                        <Box>
                          <Box as="span" color="gray.600" fontSize="sm">
                            +
                          </Box>
                          {ageLimit}
                        </Box>
                        <Box>
                          <Button
                            size="md"
                            height="48px"
                            width="200px"
                            border="2px"
                            borderColor="blue.500"
                            bgPosition="center"
                            onClick={onOpen}
                          >
                            Détails
                          </Button>

                          <Modal
                            onClose={onClose}
                            isOpen={isOpen}
                            size="xs"
                            isCentered
                          >
                            <ModalOverlay />
                            <ModalContent>
                              <ModalHeader>{name}</ModalHeader>
                              <ModalCloseButton />
                              <ModalBody>
                                <Box>{description}</Box>
                                <Box>
                                  {duration}
                                  <Box as="span" color="gray.600" fontSize="sm">
                                    min
                                  </Box>
                                </Box>
                                <Box>
                                  <Box as="span" color="gray.600" fontSize="sm">
                                    +
                                  </Box>
                                  {ageLimit}
                                </Box>
                              </ModalBody>
                              <ModalFooter>
                                <Button onClick={onClose}>Close</Button>
                              </ModalFooter>
                            </ModalContent>
                          </Modal>
                        </Box>
                      </Box>
                    </Box>
                  )
                )}
              </Stack>
            </Box>
            <footer>
              <Pagination
                isLoadingPage={isLoadingPage}
                setPage={setPage}
                page={page}
                pageSize={pageSize}
                totalItems={movies.totalItems}
              >
                <PaginationButtonFirstPage />
                <PaginationButtonPrevPage />
                <PaginationInfo flex="1" />
                <PaginationButtonNextPage />
                <PaginationButtonLastPage />
              </Pagination>
            </footer>
          </>
        )}
      </PageContent>
    </Page>
  );
};
