  import { Categorie } from "@/app/categorie/categorie.types";
  export type Movie = {
    id: number;
    name: string;
    duration:number;
    ageLimit:string;
    description:string;
    actor:string;
    image:string;
    categories: Categorie[];
  
  };
  
  export type MovieList = {
    content: Movie[];
    totalItems: number;
  };