import { Box, Heading, Stack, Text } from '@chakra-ui/react';

import { Page, PageContent } from '@/app/layout';
import { usePaginationFromUrl } from '@/app/router';

import { useCategorieList } from '@/app/categorie/categorie.service';

export const PageCategorie = () => {
  const { page } = usePaginationFromUrl();
  const pageSize = 20;
  const { data: categories } = useCategorieList({
    page: page - 1,
    size: pageSize,
  });

  return (
    <Page>
      <PageContent>
        <Heading>Catégories </Heading>
        <Box>

          <Stack direction="column" spacing={5}>
            {categories?.content.map(({id,nom, ...rest}) => (
              <Box key={id} minW="0">
                <Text noOfLines={1} maxW="full" fontWeight="bold">
                  {nom}
                  {/* <LinkOverlay as={Link} to={categorie.nom}>
                    {categorie.nom}
                  </LinkOverlay> */}
                </Text>
                <Text
                  noOfLines={1}
                  maxW="full"
                  fontSize="sm"
                  color="gray.600"
                  _dark={{ color: 'gray.300' }}
                >
                  {id}
                </Text>
              </Box>
            ))}
          </Stack>
        </Box>
      </PageContent>
    </Page>
  );
};
