  export type Categorie = {
    id: number;
    nom: string;
    films: string;
  
  };
  
  export type CategorieList = {
    content: Categorie[];
    totalItems: number;
  };