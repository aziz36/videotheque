import axios, { AxiosError } from 'axios';
import { UseQueryOptions, useQuery } from 'react-query';

import { CategorieList } from '@/app/categorie/categorie.types';

const categorieKeys = {
  all: () => ['categorieService'] as const,
  categories: ({ page, size }: { page?: number; size?: number }) =>
    [...categorieKeys.all(), 'categories', { page, size }] as const,
  categorie: ({ id }: { id?: number }) =>
    [...categorieKeys.all(), 'categorie', { id }] as const,
};

export const useCategorieList = (
  { page = 0, size = 20 } = {},
  config: UseQueryOptions<
    CategorieList,
    AxiosError,
    CategorieList,
    InferQueryKey<typeof categorieKeys.categories>
  > = {}
) => {
  return useQuery(
    categorieKeys.categories({ page, size }),
    (): Promise<CategorieList> => axios.get('/categories'),
    {
      keepPreviousData: true,
      ...config,
    }
  );
};
