import React from 'react';

import { Route, Routes } from 'react-router-dom';

import { PageCategorie } from '@/app/categorie/PageCategorie';
import { Error404 } from '@/errors';

const CategoriesRoutes = () => {
  return (
   <Routes>
      <Route path="/" element={<PageCategorie />} />
      <Route path="*" element={<Error404 />} />
    </Routes>
    
  );
};

export default CategoriesRoutes;