import { Heading } from '@chakra-ui/react';

export const Logo = ({ ...rest }) => {
  return <Heading>Vidéothéque</Heading>;
};
