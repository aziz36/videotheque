import { faker } from '@faker-js/faker';
import { Factory } from 'miragejs';


export const MovieFactory = Factory.extend({
  name: (): string => faker.word.adverb(),
  duration :(): number => faker.datatype.number(),
  ageLimit: (): number => faker.datatype.number(),
  description: (): string => faker.lorem.paragraph(),
  actor: (): string => faker.name.firstName(),
  image: (): string =>'' ,
});
